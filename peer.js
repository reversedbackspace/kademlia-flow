// @flow

const UDPServer = require('./udp-server');
const RoutingTable = require('./routing-table');
const Events = require('./events_enum');

const timeout = 5000;
const maxKBucketLength = 10;

type PeerData = {
    publicKey: number,
    ip: string,
    port: number
}


class Peer{
    publicKey: number;

    routingTable: RoutingTable;
    server: UDPServer;

    dataStore: {[number]: string};

    constructor(publicKey: number){
        //todo remove publicKey from constructor args
        this.publicKey = publicKey || Math.trunc(Math.random()*10000); //todo not crypto-secure
        this.server = new UDPServer(this.publicKey);
        this.routingTable = new RoutingTable(this.publicKey, maxKBucketLength, this.server);

        this.dataStore = {};

        this.server.on(Events.GET_CLOSEST_PEERS, msg => {
            msg.reply(Events.RETURN_CLOSEST_PEERS,this.routingTable.getClosestPeersTo(parseInt(msg.content)))
                .catch(e => {
                    console.error(`ERR! exception raised while trying to reply to 'get_closest_peers' with ${e.toString()}`);
                });
        });
        this.server.on(Events.ADDED_TO_RT, msg => {
            this.routingTable.add(msg.from).catch(e => {
                console.log(`error during an attempt to add peer to RT ${e.toString()}`);
            })
        });
        this.server.on(Events.STORE_VALUE, async msg=>{
            try{
                let [key, value] = JSON.parse(msg.content);
                this.dataStore[key] = value;
                await msg.reply(Events.VALUE_STORED, 'stored');
                console.log(`peer ${this.publicKey} saved {${key}: ${value}}`)
            }catch(e){
                console.warn(`exception while trying to store value: ${e.toString()}`);
            }


        });
        this.server.on(Events.GET_VALUE, async msg => {
            try{
                if(this.dataStore[msg.content]){
                    await msg.reply(Events.RETURN_VALUE, this.dataStore[msg.content])
                }else{
                    await msg.reply(Events.VALUE_NOT_FOUND, '404');
                }
            }catch(e){
                console.warn(`exception raised while trying to reply to GET_VALUE: ${e.toString()}`)
            }

        });
    }
    getPeerData(): PeerData{
        return {
            publicKey: this.publicKey,
            ip: this.server.getAddress().ip,
            port: this.server.getAddress().port
        }
    }

    async connect(peerData: PeerData): Promise<PeerData> {
        console.log(`connecting ${this.publicKey} to ${peerData.publicKey}`);
        try{
            await this.routingTable.add(peerData);
            return await this.protocolFindPeer(this.publicKey);
            //console.log(`first connection search result: ${JSON.stringify(searchResult)}`);
        }catch(e){
            console.warn(`first connection search failed with: ${e.toString()}`);
            throw e;
        }
    }

    async goOnline(port: number){
        try{
            await this.server.start(port);
            console.log(`peer ${this.publicKey} went online on ${port}`)
        }catch(e){
            throw e;
        }

    }
    async goOffline(){
        await this.server.stop();
    }

    async protocolPing(peerData: PeerData){
        return this.server.ping(peerData.ip, peerData.port)
    }

    async performRecursiveQueryProtocol(publicKey: number, completeChecker: (peerData: PeerData)=>Promise<boolean>){
        let alreadyQueried = [];
        let complete = false;

        let recursiveQuery = (publicKey, where): Promise<PeerData> => {
            return new Promise((resolve, reject) => {

                if(!where || where.length === 0){
                    return reject('Initiating search on an empty PeerData[] array');
                }else if(where.find(peer => !peer)){
                    console.warn('recursive query encountered some null peers');
                }

                if(complete){
                    return;
                }

                let queries = 0;
                let rejectedQueries = 0;

                Promise.all(where.filter(peer => peer && !alreadyQueried.includes(peer)).map(peerData => {
                    return new Promise(async (resolve, reject) => {
                        if(await completeChecker(peerData)){ //check if the peer we are looking for is already found
                            complete = true;
                            return resolve(peerData);
                        }

                        if(peerData.publicKey === this.publicKey){
                            return;
                        }

                        //receive peers from [peerData]
                        let receivedPeers;
                        try{
                            receivedPeers = await this._askAnotherPeerForClosestPeers(peerData, publicKey);
                            queries++;
                        }catch(e){
                            console.warn(`failed while asking for closest peers`);
                            return;
                        }
                        receivedPeers.forEach(peer => {
                            this.routingTable.add(peer).catch(e => {
                                console.warn(`exception raised while trying to add ${peer.publicKey} to ${this.publicKey}'s routing table: ${e.toString()}`);
                            })
                        });

                        try{
                            let foundPeers = await recursiveQuery(publicKey, receivedPeers); //recurrent call
                            //if the search went well
                            complete = true; //complete the search
                            return resolve(foundPeers); //and resolve the Promise with the peer found
                        }catch(e){
                            //if the search has failed
                            console.warn(`recurrent find call rejected with ${e.toString()}`);
                            rejectedQueries++; //one more rejection
                            if(rejectedQueries === queries)
                                return reject(`All queries rejected. When ${this.publicKey} tried to find ${publicKey}`);
                            else if(rejectedQueries > queries)
                                console.warn('reject count is greater that queries count')
                        }
                    })
                })).then(()=>{
                    if(queries === 0){
                        return reject('No peers to query')
                    }
                })
            });
        };

        return recursiveQuery(publicKey, this.routingTable.getClosestPeersTo(publicKey));
    }

    async protocolFindPeer(publicKey: number){
        return new Promise((resolve, reject) => {
            this.performRecursiveQueryProtocol(publicKey, async (peerData: PeerData) => {
                if(peerData.publicKey === publicKey){
                    console.log(`${this.publicKey} found ${JSON.stringify(peerData)}`);
                    resolve(peerData);
                    return true;
                }
                return false;
            }).catch(e => {
                console.warn(`search failed: ${e.toString()}`);
                reject(e);
            })
        })
    }

    async protocolFindValue(key: number){
        return new Promise((valueFound, searchReject) => {
            if(this.dataStore[key]){
                return valueFound(this.dataStore[key]);
            }

            this.performRecursiveQueryProtocol(key, (peerData: PeerData) => {
                return new Promise((resolve) => {
                    this.server.send(Events.GET_VALUE, key, peerData, response => {
                        if(response.type === Events.VALUE_NOT_FOUND){
                            resolve(false);
                        }else if(response.type === Events.RETURN_VALUE){
                            resolve(true);
                            valueFound(response.content);
                        }else{
                            console.warn(`unexpected response got from ${JSON.stringify(peerData)} for GET_VALUE: ${response.type} - ${response.content}`);
                        }
                    }).catch(e => {
                        console.warn(`exception raised while trying to GET_VALUE from ${JSON.stringify(peerData)}: ${e.toString()}`);
                        resolve(false);
                    })
                })

            }).catch(e => {
                console.warn(`search failed: ${e.toString()}`);
                searchReject(e);
            })
        })
    }

    async protocolStore(key: number, value: string|number|boolean): Promise<PeerData[]>{
        return new Promise((resolve, reject) => {
            let peersToQuery = this.routingTable.getClosestPeersTo(key);
            let peersThatAccepted = [];
            let peersThatDeclined = [];
            peersToQuery.forEach(peer => {
                this.server.send(Events.STORE_VALUE, JSON.stringify([key, value]), peer, response => {
                    if(response.type === Events.VALUE_STORED){
                        console.log(`${this.publicKey} received a response from ${JSON.stringify(response.from)}: Value Stored`)
                        peersThatAccepted.push(response.from);
                    }else if(response.type === Events.VALUE_DENIED){
                        peersThatDeclined.push(response.from);
                    }else{
                        console.warn(`peer ${JSON.stringify(peer)} responded to protocolStore in an unexpected manner: ${response.type}`);
                    }
                    if(peersThatDeclined.length + peersThatAccepted.length === peersToQuery.length){
                        resolve(peersThatAccepted);
                    }
                }).catch(e => {
                    console.error(`failed to send a store RPC to ${JSON.stringify(peer)}: ${e.toString()}`);
                    peersThatDeclined.push(peer);
                    if(peersThatDeclined.length + peersThatAccepted.length === peersToQuery.length){
                        resolve(peersThatAccepted);
                    }
                })
            });
            //todo disabled for debugging
            /*setTimeout(()=>{
                if(peersThatAccepted.length === 0){
                    reject();
                }else{
                    resolve(peersThatAccepted);
                }
            }, timeout);*/
        })

    }



    _askAnotherPeerForClosestPeers(askWho: PeerData, searchForWho: number): Promise<PeerData[]>{
        return new Promise((resolve, reject)=>{
            this.server.send(Events.GET_CLOSEST_PEERS, searchForWho, askWho, response => {
                let foundPeers: PeerData[] = [];
                try{
                    foundPeers = JSON.parse(response.content);
                }catch(e){
                    return reject(e);
                }
                if(foundPeers.length === 0)
                    return reject(`Peer ${askWho.ip}:${askWho.port} returned an a empty array of peers`);

                resolve(foundPeers);
            }).catch(reject);
            setTimeout(()=>{
                //todo disabled rejection for debugging
                //reject(`asking ${askWho.ip}:${askWho.port} for closest peers timed out: ${timeout}ms`);
            }, timeout)
        })
    }


}

module.exports = Peer;