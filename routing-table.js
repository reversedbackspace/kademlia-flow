// @flow
const _ = require('underscore');
const UDPServer = require('./udp-server');
const Events = require('./events_enum');

type PeerData = {
    publicKey: number,
    ip: string,
    port: number
}

class KBucket{

    k: number;
    bucket: PeerData[];
    server: UDPServer;

    constructor(k: number, server: UDPServer){
        this.k = k;
        this.server = server;
        this.bucket = [];
    }


    //todo consider multi-threading errors while adding or removing elements
    async addNewPeerIfNeeded(peer: PeerData){
        if(this.bucket.find(eachPeer => _.isEqual(eachPeer, peer))){
            //If the sending node already exists in the recipient's k-bucket,
            // the recipient moves it to the tail of the list
            let indexOfPeerInBucket = this.bucket.findIndex(eachPeer => _.isEqual(eachPeer, peer));
            this.bucket.splice(indexOfPeerInBucket, 1);
            this.bucket.push(peer);

        }else if(this.bucket.length < this.k){
            //If the node is not already in the appropriate k-bucket and the bucket has
            //fewer than k entries, then the recipient just inserts the new sender
            // at the tail of the list
            this.bucket.push(peer);
            this._notifyPeer(peer);
        }else{
            //if the appropriate k-bucket is full, however then
            // the recipient pings the k-bucket's least-recently seen node (head)
            let peerToPing = this.bucket[0];
            // to decide what to do:
            try{
                //Otherwise, if the least-recently seen node responds,
                // it is moved to the tail of the list, and the new sender's contact is discarded
                let ping = await this.server.ping(peerToPing.ip, peerToPing.port);
                console.log(`Peer ${peerToPing.publicKey}@${peerToPing.ip}:${peerToPing.port} pinged ${ping}ms`);
                //only if responds
                this.bucket.shift(); //remove head from array
                this.bucket.push(peerToPing);
            }catch(e){
                //if the least-recently seen node fails to respond,
                this.bucket.shift();// it is evicted from the k-bucket
                this.bucket.push(peerToPing);// and the new sender inserted at the tail
                this._notifyPeer(peer);
                console.log(`Peer ${peerToPing.publicKey}@${peerToPing.ip}:${peerToPing.port} failed to respond`);
            }

        }
    }
    _notifyPeer(peer: PeerData){
        this.server.send(Events.ADDED_TO_RT, 'notified about being added', peer).catch(console.log);
    }
}

function distance(x,y){
    return x ^ y;
}



class RoutingTable{

    publicKey: number;

    kBuckets: KBucket[];
    maxKBucketLength: number;
    server: UDPServer;


    constructor(peerId: number, maxBucketLength: number, server: UDPServer){
        this.publicKey = peerId;
        this.maxKBucketLength = maxBucketLength;
        this.kBuckets = [];
        this.server = server;
    }
    getClosestPeersTo(publicKey: number): PeerData[]{
        let kBucketNumber = RoutingTable._getBinaryDigitsCount(distance(publicKey, this.publicKey)) - 1;
        let closestPeers: PeerData[] = [];

        for(let i = kBucketNumber; i >= 0; i--){
            let currentKBucket = this.kBuckets[i];
            if(!currentKBucket) continue;
            let leftToConcat = this.maxKBucketLength - closestPeers.length; //how many peers i have to push more
            if(currentKBucket.bucket.length <= leftToConcat){
                closestPeers = closestPeers.concat(currentKBucket.bucket);
            }else if(currentKBucket.bucket.length > leftToConcat){
                closestPeers = closestPeers.concat(currentKBucket.bucket.slice(currentKBucket.bucket.length- leftToConcat, currentKBucket.bucket.length));
            }
            if(closestPeers.length === this.maxKBucketLength)
                return closestPeers;
        }
        for(let i = kBucketNumber + 1; i < this.kBuckets.length; i++){
            let currentKBucket = this.kBuckets[i];
            if(!currentKBucket) continue;
            let leftToConcat = this.maxKBucketLength - closestPeers.length ; //how many peers i have to push more
            if(currentKBucket.bucket.length <= leftToConcat){
                closestPeers = closestPeers.concat(currentKBucket.bucket);
            }else if(currentKBucket.bucket.length > leftToConcat){
                closestPeers = closestPeers.concat(currentKBucket.bucket.slice(currentKBucket.bucket.length- leftToConcat, currentKBucket.bucket.length));
            }
            if(closestPeers.length === this.maxKBucketLength)
                return closestPeers;
        }
        return closestPeers;
    }

    async add(peerData: PeerData){
        if(this.publicKey === peerData.publicKey)
            throw 'Trying to add peer to its own routing table';

        let kBucketNumber = RoutingTable._getBinaryDigitsCount( distance(peerData.publicKey, this.publicKey) ) - 1;

        if(this.kBuckets[kBucketNumber] === undefined){
            this.kBuckets[kBucketNumber] = new KBucket(this.maxKBucketLength, this.server);
        }

        await this.kBuckets[kBucketNumber].addNewPeerIfNeeded(peerData);


    }

    static _getBinaryDigitsCount(value) {
        let digits = 1;
        while ((value = value >> 1) > 0)
            digits++;
        return digits;
    }
}

module.exports = RoutingTable;