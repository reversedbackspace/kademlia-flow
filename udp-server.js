const crypto = require('crypto');
const dgram = require('dgram');
const sha256 = x => crypto.createHash('sha256').update(x, 'utf8').digest('hex');
const Socket = dgram.Socket;

type PeerData = {
    publicKey: number,
    ip: string,
    port: number
}

type Message = {
    type: string,
    content: string,
    from: PeerData,
    to: PeerData,
    reply: (event : string, msg: Object) => Promise;
}


type MessageListener = (msg: Message) => void;

class UDPServer {
    pingTimeout: number;

    publicKey: number;
    listeners: { [event: string]: MessageListener};
    contextListeners: { [hashContext: string]: MessageListener};
    socket: Socket;

    constructor(publicKey: number, pingTimeout: number = 5000) {
        this.publicKey = publicKey;
        this.pingTimeout = pingTimeout;
        this.listeners = {};
        this.contextListeners = {};
        this.socket = dgram.createSocket('udp4');

        this._setUpListeners();
    }
    _setUpListeners(){
        this.socket.on('error', (err) => {
            console.log(`Socket at ${this.socket.address().port} received an Error ${err.toString()}`);
        });
        this.socket.on('listener', ()=>{
            console.log(`socket listening on ${this.socket.address().port}`);
        });
        this.socket.on('message', (message, remoteInfo) => {
            let msg;
            try {
                msg = JSON.parse(message.toString('utf-8'));
            } catch (e) {
                throw new Error('Corrupted message received: ' + e);
            }
            if (!msg.type || !msg.from.publicKey || msg.from.ip !== remoteInfo.address || msg.from.port !== remoteInfo.port) {
                throw `Corrupted message received (without type property) ${msg}`;
            }

            msg.reply = (event: string, reply: string | Object) => {
                return this.send(event, reply, {
                    publicKey: msg.from.publicKey,
                    ip: msg.from.ip,
                    port: msg.from.port,
                    context: msg
                });
            };


            if (this.listeners[msg.type]) this.listeners[msg.type](msg);
            if (msg.context && this.contextListeners[msg.context]) {
                this.contextListeners[msg.context](msg);
                delete this.contextListeners[msg.context];
            }
        });

        this.on('PING', (message)=>{
            message.reply('PONG', 'ponging message');
        });
    }

    getAddress(){
        try{
            return {
                ip: this.socket.address().address,
                port: this.socket.address().port
            }
        }catch(e){
            return {
                ip: undefined,
                port: undefined
            }
        }

    }

    ping(publicKey: number,ip: string, port: number) {
        return new Promise((resolve, reject)=>{
            let startTime = new Date().getTime();
            this.send('PING', 'pinging message', {
                publicKey: publicKey,
                ip: ip,
                port: port
            }, msg => {
                let endTime = new Date().getTime();
                resolve(endTime - startTime);
            }).catch(reject);
            setTimeout(()=>{
                reject(this.pingTimeout);
            }, this.pingTimeout)
        });
    }

    start(port: number) {
        return new Promise((resolve, reject) => {
            try{
                this.socket.bind(port, 'localhost', () => {
                    console.log(`server ${port} started`);
                    resolve(port);
                });
            }catch(e){
                reject(e);
            }
        });
    }

    stop() {
        return new Promise((resolve, reject) => {
            try{
                this.socket.close(() => {
                    resolve(this.socket.address().port);
                });
            }catch(e){
                reject(e)
            }
        });
    }

    on(event: string, listener: MessageListener) {
        this.listeners[event] = listener;
    }
    off(event: string){
        if(this.listeners[event]){
            delete this.listeners[event];
        }else{
            throw 'Event does not exist';
        }
    }
    send(event: string, messageContent: string | Object | number,
         messageInfo: PeerData | {context?: string | Object},
         responseListener?: MessageListener): Promise {

        return new Promise((resolve, reject) => {
            messageInfo.ip = messageInfo.ip || 'localhost';

            if(!messageInfo.port)
                throw 'port is undefined';

            switch(typeof messageContent){
                case 'string':
                    break;//fine
                case 'object':
                    messageContent = JSON.stringify(messageContent);
                    break;
                case 'number':
                    messageContent = messageContent.toString();
            }

            let msg = {
                type: event,
                from: {
                    publicKey: this.publicKey,
                    ip: this.socket.address().address,
                    port: this.socket.address().port
                },
                to: {
                    publicKey: messageInfo.publicKey,
                    ip: messageInfo.ip,
                    port: messageInfo.port
                },
                content: messageContent
            };

            if(messageInfo.context){
                if(typeof messageInfo.context === 'string'){
                    msg.context = sha256(messageInfo.context);
                }else if(typeof messageInfo.context === 'object'){
                    msg.context = sha256(JSON.stringify(messageInfo.context));
                }else{
                    throw 'context must be string or Object';
                }
            }
            if (responseListener) {
                let stringMessage = JSON.stringify(msg);
                this.contextListeners[sha256(stringMessage)] = responseListener;
            }

            this.socket.send(JSON.stringify(msg), messageInfo.port, messageInfo.ip, () => {
                resolve(msg);
            });
        });
    }
}

module.exports = UDPServer;