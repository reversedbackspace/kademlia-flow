module.exports = {
    GET_CLOSEST_PEERS: 'get_closest_peers',
    RETURN_CLOSEST_PEERS: 'return_closest_peers',

    ADDED_TO_RT: 'added_to_rt',

    STORE_VALUE: 'store_value',
    VALUE_STORED: 'store_value_confirmation',
    VALUE_DENIED: 'store_value_denial',

    GET_VALUE: 'get_value',
    VALUE_NOT_FOUND: 'value_not_found',
    RETURN_VALUE: 'return_value'
};