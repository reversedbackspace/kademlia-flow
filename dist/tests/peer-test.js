const Peer = require('../peer');

let peers = [];
for (let i = 0; i < 6; i++) {
    peers[i] = new Peer(i + 1);
}

async function test() {
    for (let i = 0; i < peers.length; i++) {
        try {
            await peers[i].goOnline(1000 + i);
        } catch (e) {
            console.log(`${i} failed to start because of ${e.toString()}`);
        }
    }

    peers.forEach((each, index) => {
        console.log(`${index}) ${JSON.stringify(each.getPeerData())}`);
    });

    try {
        for (let i = 0; i < peers.length; i++) {
            if (i === 0) continue;
            let searchResult = await peers[i].connect(peers[i - 1].getPeerData());
            console.log('SEARCH COMPLETED: ' + searchResult);
        }
    } catch (e) {
        console.log('failed');
        console.log(e);
    }
}
test().then(() => {
    setTimeout(async () => {
        peers.forEach((peer, index) => {
            console.log(`Routing table of ${JSON.stringify(peer.getPeerData())}`);
            console.log(peer.routingTable.kBuckets.map(kbucket => kbucket.bucket));
        });
        console.log('--storing value');
        let storingPeers = await peers[0].protocolStore(101, 'shizz');
        console.log(storingPeers);
        console.log('--removing value from last peers dataStore');
        delete peers[5].dataStore[101];
        console.log('--deleted initiating search');
        let foundValue = await peers[5].protocolFindValue(101);
        console.log('VALUE FOUND: ' + foundValue);
    }, 5000);
}).catch(console.log);